/*jshint browser:true, devel:true evil:true*/
(function () {

// helpers.js
//------------------------------
//
// 2012-10-12, Jonas Colmsjö
//
// Copyright 2012 Gizur AB 
//
// Examples for the Gizur REST API
//
// dependencies: npm install jsdom xmlhttprequest jQuery optimist
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
// The code is commented in 'docco style' - http://jashkenas.github.com/docco/ 
//
//------------------------------

    "use strict";

    // Local libraries
    // ---------------


    // Generic Helper Functions
    //==========================


    // Unit test functions
    //------------------
    // Based on John Resig's Ninja book

    var results;
    
    global.assert = function assert( value, desc ) {
        var li = document.createElement("li");
        li.className = value ? "pass" : "fail";
        li.appendChild( document.createTextNode( desc ) );
        results.appendChild( li );
        if ( !value ) {
            li.parentNode.parentNode.className = "fail";
        }
        return li;
    };
    
    global.test = function test(name, fn) {
        results = document.getElementById("results");
        results = global.assert( true, name ).appendChild(
            document.createElement("ul") );
        fn();
    };
    

    // Logging
    //------------------
    
    global.logging = {
        emerg:   0,
        alert:   1,
        crit:    2,
        err:     3,
        warning: 4,
        notice:  5,
        info:    6,
        debug:   7
    };

    global.logging.threshold = global.logging.warning;


    global.logEmerg = function(){
        if(global.logging.emerg <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logAlert = function(){
        if(global.logging.alert <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logCrit = function(){
        if(global.logging.crit <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logErr = function(){
        if(global.logging.err <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logWarning = function(){
        if(global.logging.warning <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logNotice = function(){
        if(global.logging.notice <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logInfo = function(){
        if(global.logging.info <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    global.logDebug = function(){
        if(global.logging.debug <= global.logging.threshold ) {
            console.log(arguments);
        }
    };

    // A simple logging statement that works in all browsers.
    global.log = function log() {
        try {
            console.log.apply( console, arguments );
        } catch(e) {
            try {
                opera.postError.apply( opera, arguments );
            } catch(e){
                alert( Array.prototype.join.call( arguments, " " ) );
            }
        }
    };


    // Templating
    //------------------

    // Simple JavaScript Templating
    // John Resig - http://ejohn.org/ - MIT Licensed 
    var cache = {};
    global.tmpl = function tmpl(str, data){
    
        // Figure out if we're getting a template, or if we need to 
        // load the template - and be sure to cache the result. 
        var fn = !/\W/.test(str) ?
            cache[str] = cache[str] || tmpl(document.getElementById(str).innerHTML) :
        
        // Generate a reusable function that will serve as a template 
        // generator (and which will be cached).
        new Function(
            "obj","var p=[],print=function(){p.push.apply(p,arguments);};" + 
            
            // Introduce the data as local variables using with(){}
            "with(obj){p.push('" +
            
            // Convert the template into pure JavaScript
            str
                .replace(/[\r\t\n]/g, " ")
                .split("<%").join("\t")
                .replace(/((^|%>)[^\t]*)'/g, "$1\r")
                .replace(/\t=(.*?)%>/g, "',$1,'")
                .split("\t").join("');")
                .split("%>").join("p.push('")
                .split("\r").join("\\'") + 
                "');}" + 
                "return p.join('');"
            );
        
        // Provide some basic currying to the user
        return data ? fn( data ) : fn; 
    };


    // ISODateString
    //-------------------------------------------------------------------------------------------------
    //

    global.ISODateString = function(d) {

        return d.toISOString();
        
        function pad(n){
            return n < 10 ? '0' + n : n;
        }

        return d.getUTCFullYear()  + '-' + 
            pad(d.getUTCMonth()+1) + '-' + 
            pad(d.getUTCDate())    + 'T' +
            pad(d.getUTCHours())   + ':' + 
            pad(d.getUTCMinutes()) + ':' +
    //          pad(d.getUTCSeconds())+'Z'
            pad(d.getUTCSeconds()) +
            (d.getTimezoneOffset() < 0 ? '+' : '-') +
            pad(Math.abs(Math.floor(d.getTimezoneOffset() / 60 ))) + ':' +
            pad(Math.abs(d.getTimezoneOffset()) % 60) ;
    };


    // Gizur API Helper Functions
    //==========================


    // signString
    //-------------------------------------------------------------------------------------------------
    //

    global.signString = function (stringToSign, secret) {

        // Generate the hash
        var shaObj = new global.jsSHA(stringToSign, "ASCII");
        var hmac   = shaObj.getHMAC(secret, "TEXT", "SHA-256", "B64");

        // Encode in bas64 using jQuery plugin
        //var encoded = $.base64.encode( hmac );

        // usefull for debuggning
        global.logDebug('sign: stringToSign - ' + stringToSign + ' - hash - ' + hmac );

        return hmac;
    }

    // sign
    //-------------------------------------------------------------------------------------------------
    //

    global.sign = function (model, method, key, secret, delta) {

        // Get Current UNIX time
        var unixtimestamp = new Date().getTime();

        // Check if delta is defined
        if (delta==undefined) delta = 0;

        // The ISO-8601 date 
        var timestamp = global.ISODateString(new Date(unixtimestamp + (delta * 1000)));

        // I'm using a 10 digit random number as salt
        var salt     =  Math.floor( Math.random() * 1000000000 );

        // Build the string to sign
        var signatureArray =[ 
                'Verb' +       method,
                'Model' +      model,
                'Version' +    '0.1',
                'Timestamp' +  timestamp,
                'KeyID' +      key,
                'UniqueSalt' + salt 
            ];

        // Sort the array
        signatureArray.sort();

        // Create a string out of array
        var stringToSign = signatureArray.join('');

        var encoded = signString(stringToSign, secret);

        // return a object with timestamp, salt and the signture
        return { timestamp: timestamp, salt: salt, base64: encoded };
    };


}());
