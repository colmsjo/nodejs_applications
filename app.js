
//------------------------------
//
// 2012-10-18, Jonas Colmsjö
//
// Copyright Gizur AB 2012
//
// Functions:
//  * Schedule jobs cron-style
//  * Provide a central logging function to be used by applications
//
// Install with dependencies: npm install 
//
// Documentation is 'docco style' - http://jashkenas.github.com/docco/
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------


"use strict";


// Includes
// =========

var express = require('express');
var http    = require('http');
var sha     = require('./lib/sha256.js');
var cronJob = require('./lib/cron').CronJob;
var helper  = require('./lib/helper');

var recur = require('later').recur
    , cron = require('later').cronParser
    , text = require('later').enParser
    , later = require('later').later;
    
var url = require("url.js");
var querystring = require('querystring');

// Variables
// =========

var cronlog    = [],
    cronSchedulers   = [],
    counter    = 0,

    // Number of rows to keep in the log
    rowsToShow = 1000,

    // Port to use when no running in heroku etc.
    serverPort = 5000,

    // Use the port specified unless for istance heroku already has assinged one
    port = process.env.PORT || serverPort,

    // Credentials for logging in to Gizur
    config  = require('./config.js').Config,

    // The URL of the app, only used when refreshing the log page in the browser
    //    serverURL  = 'http://gizur-cron.herokuapp.com/';

    serverURL  = (process.env.PORT) ? config.HEROKU_URL + '/log' : 'http://localhost:' + port +'/log',
    
    // Initialize the HOST variables
    // These variable will be overrided as per the environment variable.
   
    // Tells weather to make secure calls to the HOST
    IS_HTTPS = false,
    // Port to communicate
    SERVER_PORT = 80,
    // HOST
    HOSTNAME = '';

// Variable are being set as per the process.env.NODE\_ENV
// value.
// You can set process.env.NODE\_ENV variable by following command.

// _heroku config:set NODE\_ENV=development_

// _heroku config:set NODE\_ENV=qa_

// _heroku config:set NODE\_ENV=production_

// In case of production environment.

if(process.env.NODE_ENV == 'gc1-ireland'){
    IS_HTTPS = true;
    SERVER_PORT = 443;
    HOSTNAME = 'gizur.com';
}
    
// In case of qa environment.

if(process.env.NODE_ENV == 'gc2-ireland'){
    IS_HTTPS = true;
    SERVER_PORT = 443;
    HOSTNAME = 'c2.gizur.com';
}

// In case of development environment.

if(process.env.NODE_ENV == 'gc3-ireland' || HOSTNAME == '')
    HOSTNAME = 'phpapplications-env-sixmtjkbzs.elasticbeanstalk.com';
    

//If cron host require secure connection.
if(IS_HTTPS)
    http = require('https');

// Setup jobs to run
// ================================

// Heartbeat
// -----------------

function runThis(){
    return JSON.stringify({ 
        "client"    : "server",
        "timestamp" : new Date().toLocaleString(),
        "message"  : "You will see this message every minute" 
    });
}

// To validate the json string
// ----------------------------

function isValidJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// Run a Batch File
// ================

// Accept three parameters
// client : Used in logging,
// host   : Hostname,
// batch  : path to file

function hitBatch(client, batchurl, method, params){
    
    var _url = url.parse(batchurl);
    
    var port = 80;
    if(_url.scheme === 'https'){
        http = require('https');
        port = 443;
    }
    
    var post_data = url.buildget(params);
    
    var _headers = {};
    
    if(method !== 'GET'){
        _headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': post_data.length
        };
    }
    // Set the options
    var options = {
        hostname: _url.host,
        port: port,
        path: _url.path + "?" + _url.query,
        method: method,
        headers: _headers
    };

    // Request the host
    var req = http.request(options, function(res) {
        var body = '';
        // On success
        res.on('data', function (chunk) {
            res.setEncoding('utf8');
            body += chunk.toString();
        });
        
        res.on('end', function (){
            // Log the response
            logMessage(JSON.stringify({ 
                "client"    : client,
                "timestamp" : new Date().toLocaleString(),
                "message"  : "Success",
                "response" : body
            }));
        });
    });
    
    // In case of error,
    // log the error message
    req.on('error', function(e) {
        logMessage(JSON.stringify({ 
            "client"    : client,
            "timestamp" : new Date().toLocaleString(),
            "message"  : "Error while processing batch",
            "response"  : e.message 
        }));
    });
    
    if(method !== 'GET')
        req.write(post_data);
    //End the request
    req.end();
}

// Trailer App Backup
// -----------------

function hitREST(model, action, method, apikey, secretkey) {

    var signature = global.sign(model, method, apikey, secretkey, 0);
    
    var options = {
        'hostname': HOSTNAME,
        'port': SERVER_PORT,
        'path': '/api/' + model + '/' + action,
        'method': method,
        'headers':  {

                //ISO 8601 format must be sync with the server with max -10 seconds error
                'x_timestamp': signature.timestamp,

                // The API Key is fetched from config.js
                'x_gizurcloud_api_key': apikey,

                // Base64 encoded signature
                'x_signature': signature.base64,

                'x_unique_salt': signature.salt,

                'accept': 'text/json'
        } 
    };

     var req = http.request(options, function(res) {
        var body = '';
        res.on('data', function (chunk) {
            res.setEncoding('utf8');
            body += chunk.toString();   
        });

        res.on('end', function (){
            // Save message from job
            logMessage(
                JSON.stringify({ 
                    "client"    : "server",
                    "timestamp" : new Date().toLocaleString(),
                    "message"  : "Taking MySQL backup response: ",
                    "response" : body
                })
            );
        });
    });   
    
    req.on('error', function(e) {
        logMessage(
            JSON.stringify({ 
                    "client"    : "server",
                    "timestamp" : new Date().toLocaleString(),
                    "message"  : "Error while taking MySQL backup response: ",
                    "response" : e.message 
                })
            );
    });
    
    req.end();

    return JSON.stringify({ 
        "client"    : "server",
        "timestamp" : new Date().toLocaleString(),
        "message"  : "Taking MySQL backup" 
    });
    
}

// Cron Jobs Block start
// ---------------

// Schedule heartbeat cron job to run every week on sunday
new cronJob('00 14 * * SUN', function(){


    //Create AWS object and set config
    var AWS = require('aws-sdk');

    AWS.config.update({
        accessKeyId: config.AWS_API_KEY,
        secretAccessKey: config.AWS_SECRET_KEY,
        region: "eu-west-1"
    });

    //Create DynamoDB Object and set config
    var db = new AWS.DynamoDB();
    var params = {
        "TableName" : "GIZUR_ACCOUNTS",
        "AttributesToGet" : ['apikey_1', 'secretkey_1']
    };    

    db.scan (params, function (err, data) {
        for (var index in data.Items){

            var apikey = data.Items[index].apikey_1['S'];
            var secretkey = data.Items[index].secretkey_1['S'];

            setTimeout(function (apikey, secretkey) {
                var msg = hitREST('Cron', 'dbbackup', 'PUT', apikey, secretkey);

                // Log message from job
                logMessage(msg);
            }.bind(this, apikey).bind(this, secretkey), 15000 * index);
        }
    });

}, null, true, "Europe/Stockholm");


// Schedule heartbeat cron job to run every minutes
new cronJob('1 * * * * *', function(){
    var msg = runThis();

    // Log message from job
    logMessage(msg);

}, null, true, "Europe/Stockholm");

// Run Cron Jobs
// =============
// 
// Schedule heartbeat cron job to run every seconds.
// It iterate cronSchedlers every second and match schedule (time).
// If schedule matches, hit the corresponding URL.
//
new cronJob('* * * * * *', function(){    
    if(cronSchedulers.success){
        cronSchedulers.result.forEach(function(batch, index, array) {
            var cSched = cron().parse(batch.schedule);
            if (later(1, true).isValid(cSched, new Date()) && batch.active === "1") {
                var params = JSON.parse(batch.params);
                hitBatch(batch.id, batch.url, batch.method, params);
            }
        });
    }
}, null, true, "Europe/Stockholm");

// Sync Time from DynamoDB
// =======================
// 
// Schedule heartbeat cron job to run every minute.
// This cron sync schedulars and update local cronSchedulers
// variable.
new cronJob('1 * * * * *', function(){
    
    var _url = url.parse(config.REST_API_URL);
    
    var port = 80;
    if(_url.scheme === 'https'){
        http = require('https');
        port = 443;
    }
    
    // Set the options
    var options = {
        hostname: _url.host,
        port: port,
        path: _url.path,
        method: 'GET',
        //'headers':  {
        //    'accept': 'text/plain'
        //} 
    };

    // Request the host
    var req = http.request(options, function(res) {
        var body = '';
        // On success
        res.on('data', function (chunk) {
            res.setEncoding('utf8');
            body += chunk.toString(); 
            //console.log("chunk : " + chunk.toString());
        });
        
        res.on('end', function (){
            // Log the response
            if(isValidJsonString(JSON.stringify(body)) && JSON.parse(body).success) {
                cronSchedulers = JSON.parse(body);
                logMessage(JSON.stringify({ 
                    "client"    : 'rest-api',
                    "timestamp" : new Date().toLocaleString(),
                    "message"  : "Schedulars updated successfully!"
                }));
            } else {
                //console.log("error body : " + body);
                logMessage(JSON.stringify({ 
                    "client"    : 'rest-api',
                    "timestamp" : new Date().toLocaleString(),
                    "message"  : "Schedulars updation failed!"
                }));
            }
            
        });
    });
    
    // In case of error,
    // log the error message
    req.on('error', function(e) {
        logMessage(JSON.stringify({ 
            "client"    : HOSTNAME,
            "timestamp" : new Date().toLocaleString(),
            "message"  : "Error while fetching schedulars",
            "response"  : e.message 
        }));
    });
    
    //End the request
    req.end();

}, null, true, "Europe/Stockholm");


// Web Server that displays the log
// ================================

var app = express();


function logMessage(msg) {

    // Save message to log
    cronlog.push( msg );

    // Print to console
    console.log( msg );
    
    // Track size of log
    counter++;

    // Keep size of cronlog under control
    if (counter > rowsToShow) {
        //cronlog.pop();
        cronlog.shift();
        counter--;
    }
}

// Support JSON, urlencoded, and multipart requests
app.use(express.bodyParser());


// Show cronlog in web server
app.get('/log', function(request, response) {
    var html = '<html><meta http-equiv="refresh" content="5; URL=' + serverURL + '">' +
    '<body>' +
    '<h1>Cronlog!</h1>' +
    cronlog.join('<br>') + 
    '</body></html>';
    //console.log(HOSTNAME + ' : ' + process.env.NODE_ENV);
    response.send(html);
});

// Send CSV file
app.get('/csv', function(request, response) {

    // output headers so that the file is downloaded rather than displayed

    // header('Content-Type: text/csv; charset=utf-8');
    response.type('Content-Type: text/csv; charset=utf-8');

    // header('Content-Disposition: attachment; filename=data.csv');
    response.attachment('log.csv');

    // Send the log comma separated
    response.send( cronlog.join('\n') );
});



// REST API
// =========


// Centralized logging
// -----------------------------------------------



// Store message in log
app.post('/log', function(request, response) {

    // Save message to log
    if(isValidJsonString(request.body))
        logMessage( JSON.stringify(request.body) );

    // Send response back to the client
    response.send(200, {
        "msg":"ok!"
    } ); 

});


//
// Send a message using AWS SES
//-----------------------------
// For awssum documentation see, https://github.com/appsattic/node-awssum
// Examples are found here: https://github.com/appsattic/node-awssum/tree/master/examples/amazon/ses

app.post('/sendmail', function(request, response) {

    // Used for formatting output to the log
    var fmt             = require('fmt'),

    // Library for using the AWS REST API
    awssum          = require('awssum'),
    amazon          = awssum.load('amazon/amazon'),
    Ses             = awssum.load('amazon/ses').Ses,

    // The credentials for AWS, defined in config.js
    awsAccountId    = config.AWS_ACCOUNT_ID,
    accessKeyId     = config.AWS_API_KEY,
    secretAccessKey = config.AWS_SECRET_KEY;

    // Create an Object from the awssum library for AWS SES
    var ses = new Ses({
        'accessKeyId'     : accessKeyId,
        'secretAccessKey' : secretAccessKey
    });

    fmt.dump(request);

    // Log the input for SES
    var msgToLog = '/sendmail2: ' + JSON.stringify(request.body) + ' request.req.ip: ' + request.ip + ' request.ips: ' + request.ips +
    ';;Region:'           + ses.region() + 
    ' EndPoint:'        + ses.host() +
    ' AccessKeyId:'     + ses.accessKeyId().substr(0, 3) + '...' + 
    ' SecretAccessKey:' + ses.secretAccessKey().substr(0, 3) + '...' + 
    ' AwsAccountId'     + ses.awsAccountId();

    logMessage(msgToLog);


    // Check that all fields have been supplied
    if( request.body.to      === undefined || 
        request.body.from    === undefined || 
        request.body.subject === undefined || 
        request.body.body    === undefined || 
        request.body.replyTo === undefined) {

        // Send response back to the client
        response.send(400, {
            "msg":"error! All fields not supplied"
        } );

        return;
    }

    // The input data for SES, to, subject, contents etc.
    var data = {
        ToAddresses : [
        request.body.to
        ],
        CcAddresses :    [],
        BccAddresses :   [],
        Text :           request.body.body,
        TextCharset :    'UTF-8',
        Html :           '<p>' + request.body.body + '</p>',
        HtmlCharset :    'UTF-8',
        Subject :        request.body.subject,
        SubjectCharset : 'UTF-8',
        Source :         request.body.from
    };

    // Now we're ready to send the message
    ses.SendEmail(data, function(err, data) {
        fmt.msg("sending an email - expecting success");
        fmt.msg(data, 'Data');

        if (err) {

            // Save message to log
            logMessage( JSON.stringify(err) );

            // fmt.dump(err, 'Error');                 /* Seams to work better with nested JSON */

            // Send response back to the client
            response.json(400, {
                msg: 'SES Error:' + JSON.stringify(err)
            } );

        } else {
            // Save message to log
            logMessage( JSON.stringify(data) );

            // Send response back to the client
            response.json(200, {
                msg : 'ok!'
            } );
        }
    });


});

// For Health checker
//----------------------
app.get('/', function(request, response){
        response.send(200, {
            "msg":"Everything is ok"
        } );
});
// Start the web server
// ---------------------

app.listen(port, function() {
    logMessage("Listening on " + port);
});
