// test-helpers.js
//------------------------------
//
// 2012-11-08, Jonas Colmsjö
//
// Copyright 2012 Gizur AB 
//
// Tests for helper functions
//
// dependencies: npm install jsdom xmlhttprequest jQuery optimist
// local dependencies: sha256.js, BigInt.js
// browser dependencies: jQuery
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
// The code is commented in 'docco style' - http://jashkenas.github.com/docco/ 
//
//------------------------------

"use strict";


// Includes
// =========

// The testing framework
var testCase  = require('nodeunit').testCase;

// External dependencies not managed via npm
var sha       = require('../lib/sha256.js');
var bigint    = require('../lib/BigInt.js');

// The functions to test
var helpers   = require('../node_modules/helpers/helpers.js').create();


// Test cases
// ==========

module.exports = testCase({
    "generateApiKeyAndSecret": function(test) {
        var credendials =  helpers.generateApiKeyAndSecret(bigint);

        test.ok(credendials.apiKey.length    === 13, "apiKey length should be 13");
        test.ok(credendials.apiSecret.length === 22, "apiSecret length should be 22");
        test.done();
    },

    "logging": function(test) {
        helpers.logEmerg("EMERGENCY: This message should be printed (since logging threshold is at warning");
        helpers.logInfo("TINFO: his message should NOT be printed (since logging threshold is at warning");

        helpers.logging_threshold = helpers.logging.debug;
        helpers.logInfo("DEBUG: his message should  be printed (since logging threshold now is at debug");

        test.ok(true);
        test.done();
    }

});