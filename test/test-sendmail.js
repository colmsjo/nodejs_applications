
// test-sendmail.js
//------------------------------
//
// 2012-10-22, Jonas Colmsjö
//
// Copyright Gizur AB 2012
//
// Simple test of the sendmail REST fucntion
//
// node dependencies: npm install jsdom xmlhttprequest jQuery optimist
//
// Documentation is 'docco style' - http://jashkenas.github.com/docco/
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------



/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

"use strict";


// Includes
// ================

var nodeunit = require('nodeunit');
var $        = require('jQuery');
var helpers  = require('../lib/helpers-1.0.js');
        
// Credentials for logging in to Gizur
var config  = require('../config.js').Config;


exports['test-sendmail'] = nodeunit.testCase( {

  setUp: function(callback) {

    // set logging level
    logging.threshold  = logging.debug;


    // Globals
    //==============

    var localURL    = config.LOCAL_URL + '/sendmail',
        remoteURL   = config.HEROKU_URL + '/sendmail';
 

    //
    // Send a message using AWS ES
    //-----------------------------
    // For aws2js documentation see, https://github.com/SaltwaterC/aws2js/wiki/SES-Client

    this.testSendMessage = function testSendMessage(test, to, from, subject, body, replyTo, local){

        logDebug('sendMessage: ' + body + ' at:' + ((local===undefined) ? remoteURL : localURL) );

        var request = $.ajax({

            // REST function to use
            url:      (local===undefined) ? remoteURL : localURL,
            type:     'POST',
            dataType: 'json',
            //headers:  {'Accept':'text','Content-Type':'application/json'},
            data:     {
                "to":        to,
                "from":      from,
                "subject":   subject,
                "body":      body, 
                "replyTo":   replyTo 
            },

            success: function(data){
                test.ok(true, 'sendMessage: Yea, it worked...' + JSON.stringify(data) );
                test.done();
             },

            error: function(data){
                test.ok(false, 'sendMessage: Shit hit the fan...' + JSON.stringify(data) );
                test.done();
            }

        });

        return request;

    }

    callback();

  },

  'Sending mail using local server (make sure it is up)': function(test) {
    test.expect(1);

    // tests here
    this.testSendMessage(test, 'jonas.colmsjo@gizur.com', 'noreply@gizur.com', 'TESTING SES', 'MESSAGE FROM UNIT TESTS' , 'jonas.colmsjo@gizur.com', true);
  },

  'Sending mail using remote server (make sure it is up)': function(test) {
    test.expect(1);

    // tests here
    this.testSendMessage(test, 'jonas.colmsjo@gizur.com', 'noreply@gizur.com', 'TESTING SES', 'MESSAGE FROM UNIT TESTS', 'jonas.colmsjo@gizur.com');
  }


});
