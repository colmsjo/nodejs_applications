# gizur-cron

Schedule batches using node-cron


## Getting Started
Install the module with: `npm install gizur-cron`

Run on heroku
```javascript
# the account needs to be setup
# run heroku login, if you haven't done it before

# create a new app
heroku create

# push git repo
git push heroku master
...
       Procfile declares types -> web
-----> Compiled slug size: 6.0MB
-----> Launching... done, v3
       http://nameless-spire-9094.herokuapp.com deployed to Heroku
...


# Update app.js with the URL you received

heroku logs
heroku open
```

## Documentation
The full documentation can be found at: https://github.com/gizur/gizurcloud/wiki


## Examples
_(Coming soon)_


## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [grunt](https://github.com/gruntjs/grunt).


## Release History
_(Nothing yet)_


## License
Copyright (c) 2012 Jonas Colmsjö  
Licensed under the MIT license.
